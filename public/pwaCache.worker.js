const CACHE_NAME = "parapluesch";
const ASSETS = [
    "./",
    "anstalt_kore.swf",
    "buch_as2.swf",
    "depression.swf",
    "der_rohrschachtest.swf",
    "deut.txt",
    "engl.txt",
    "favicon.ico",
    "fran.txt",
    "icon.png",
    "index.html",
    "info_board.swf",
    "ital.txt",
    "korr.txt",
    "manifest.json",
    "offtext_deut.swf",
    "offtext_engl.swf",
    "offtext_fran.swf",
    "offtext_ital.swf",
    "offtext_kore.swf",
    "offtext_russ.swf",
    "offtext_span.swf",
    "puzzle.swf",
    "pwaCache.worker.js",
    "russ.txt",
    "schoene-bescherung.swf",
    "span.txt",
    "transporttest.swf",
    "patienten/dolly_as2.swf",
    "patienten/dr_wood_as2.swf",
    "patienten/dub_as2.swf",
    "patienten/kroko_as2.swf",
    "patienten/lilo_as2.swf",
    "patienten/sly_as2.swf",
    "ruffle/ruffle.js",
];

self.addEventListener("install", installEvent => {
    installEvent.waitUntil(caches.open(CACHE_NAME)
        .then(cache => {
            cache.addAll(ASSETS);
        }));
});

self.addEventListener("fetch", fetchEvent => {
    fetchEvent.respondWith(
        new Promise((resolve, reject) => {
            fetch(fetchEvent.request)
                .then(response => {
                    caches.open(CACHE_NAME)
                        .then(cache => {
                            cache.put(fetchEvent.request, response.clone());
                            resolve(response);
                        })
                })
                .catch(() => {
                    resolve(caches.match(fetchEvent.request));
                })
        }));
});
